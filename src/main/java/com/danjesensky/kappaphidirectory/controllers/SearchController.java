package com.danjesensky.kappaphidirectory.controllers;

import com.danjesensky.kappaphidirectory.models.BrotherModel;
import com.danjesensky.kappaphidirectory.models.DataModel;
import com.danjesensky.kappaphidirectory.models.ErrorModel;
import com.danjesensky.kappaphidirectory.util.DataSourceUtil;
import com.danjesensky.kappaphidirectory.util.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RestController
public class SearchController {
    @RequestMapping(value = "/api/Search/{search}", method = RequestMethod.GET)
    public ResponseEntity searchBrothers(@PathVariable String search){
        List<BrotherModel> model;
        try(Connection c = DataSourceUtil.getInstance().getDatasource().getConnection()){
            String sql = "select `id`,`firstName`,`lastName`,`dateJoined` from `brother` where (`brother`.`firstname` like ? or `brother`.`lastname` like ?) and `brother`.`dateJoined` is not null limit 0,2000";
            String[] split = search.split(" ", 2);
            boolean isFullName = split.length > 1;

            if(isFullName){
                sql = sql.replaceFirst("or", "and");
            }

            try(PreparedStatement p = c.prepareStatement(sql)) {
                //wildcard both sides of each string
                p.setString(1, "%"+split[0]+"%");
                if(isFullName){
                    p.setString(2, "%"+split[1]+"%");
                }else{
                    p.setString(2, "%"+split[0]+"%");
                }

                p.execute();

                try(ResultSet r = p.getResultSet()) {
                    model = BrothersController.extractBrotherList(r);
                }
            }
        }catch(SQLException e){
            System.err.println(e.getMessage());
            return ResponseUtil.CreateCorsResponse(new ErrorModel(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseUtil.CreateCorsResponse(new DataModel<>(model), HttpStatus.OK);
    }
}
