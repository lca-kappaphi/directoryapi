package com.danjesensky.kappaphidirectory.controllers;

import com.danjesensky.kappaphidirectory.models.AnsweredQuestionModel;
import com.danjesensky.kappaphidirectory.models.BrotherModel;
import com.danjesensky.kappaphidirectory.models.DataModel;
import com.danjesensky.kappaphidirectory.models.ErrorModel;
import com.danjesensky.kappaphidirectory.util.DataSourceUtil;
import com.danjesensky.kappaphidirectory.util.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BrothersController {
    @RequestMapping(value = "/api/Brothers", method = RequestMethod.GET)
    public ResponseEntity getBrothers(){
        List<BrotherModel> model;

        try(Connection c = DataSourceUtil.getInstance().getDatasource().getConnection()){
            PreparedStatement p = c.prepareStatement("select `id`,`firstName`,`lastName`,`dateJoined` from `brother` where `dateJoined` is not null and `expectedGraduation` > now() and not exists (select 1 from `inactive_brother` where `brother`.`id`=`inactive_brother`.`id`) order by `brother`.`zetaNumber`");
            p.execute();
            ResultSet r = p.getResultSet();

            model = extractBrotherList(r);

            r.close();
            p.close();
            c.close();
        }catch(SQLException e){
            System.err.println(e.getMessage());
            return ResponseUtil.CreateCorsResponse(new ErrorModel(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseUtil.CreateCorsResponse(new DataModel<>(model), HttpStatus.OK);
    }

    public static List<BrotherModel> extractBrotherList(ResultSet r) throws SQLException{
        List<BrotherModel> model = new ArrayList<>();
        while(r.next()){
            BrotherModel m = new BrotherModel();
            m.ID = r.getInt(1);
            m.Name = r.getString(2)+" "+r.getString(3);
            m.DateJoined = r.getDate(4);
            model.add(m);
        }
        return model;
    }

    @RequestMapping(path = "/api/Brothers/{id}", method = RequestMethod.GET)
    public ResponseEntity getBrother(@PathVariable int id){
        try(Connection c = DataSourceUtil.getInstance().getDatasource().getConnection()){
            BrotherModel model = new BrotherModel();
            ResultSet r;
            int bigBrotherID;

            PreparedStatement p = c.prepareStatement("select `id`,`firstName`,`lastName`,`dateJoined`,`dateInitiated`,`zetaNumber`,`expectedGraduation`,`bigBrotherID` from `brother` where `id`=?");
            p.setInt(1, id);
            p.execute();
            r = p.getResultSet();

            if(!r.next()){
                return ResponseUtil.CreateCorsResponse(new ErrorModel("No such brother found with ID "+id), HttpStatus.NOT_FOUND);
            }

            model.ID = r.getInt(1);
            model.Name = r.getString(2)+" "+r.getString(3);
            model.DateJoined = r.getDate(4);
            model.DateInitiated = r.getDate(5);
            model.ZetaNumber = r.getInt(6);
            model.ExpectedGraduation = r.getDate(7);
            bigBrotherID = r.getInt(8);

            r.close();
            p.close();

            p = c.prepareStatement("select `questionText`,`answerText` from `answer` inner join `question` on `question`.`ID`=`answer`.`questionID` where `brotherID`=?");
            p.setInt(1, id);
            p.execute();
            r = p.getResultSet();

            while(r.next()){
                model.Questions.add(new AnsweredQuestionModel(r.getString("questionText"), r.getString("answerText")));
            }
            r.close();
            p.close();

            p = c.prepareStatement("select `major`.`name` from `major` inner join `brother_major` on `major`.`ID`=`brother_major`.`majorID` where `brotherID`=?");
            p.setInt(1, id);
            p.execute();
            r = p.getResultSet();

            while(r.next()){
                model.Majors.add(r.getString("name"));
            }
            r.close();
            p.close();

            p = c.prepareStatement("select `minor`.`name` from `minor` inner join `brother_minor` on `minor`.`ID`=`brother_minor`.`minorID` where `brotherID`=?");
            p.setInt(1, id);
            p.execute();
            r = p.getResultSet();

            while(r.next()){
                model.Minors.add(r.getString("name"));
            }
            r.close();
            p.close();

            p = c.prepareStatement("select `position`.`name` from `position` inner join `brother_position` on `brother_position`.`positionID`=`position`.`id` where `brother_position`.`brotherID`=? and CURDATE() between `brother_position`.`start` and `brother_position`.`end`");
            p.setInt(1, id);
            p.execute();
            r = p.getResultSet();

            while(r.next()){
                model.CurrentPositions.add(r.getString("name"));
            }
            r.close();
            p.close();

            p = c.prepareStatement("select `position`.`name` from `position` inner join `brother_position` on `brother_position`.`positionID`=`position`.`id` where `brother_position`.`brotherID`=? and CURDATE() > `brother_position`.`end`");
            p.setInt(1, id);
            p.execute();
            r = p.getResultSet();

            while(r.next()){
                model.PastPositions.add(r.getString("name"));
            }
            r.close();
            p.close();

            //bigs
            p = c.prepareStatement("select `brother`.`firstname`,`brother`.`lastname` from `brother` where `brother`.id=?");
            p.setInt(1, bigBrotherID);
            p.execute();
            r = p.getResultSet();

            while(r.next()){
                model.BigBrother = r.getString("firstname") + ' ' + r.getString("lastname");
            }
            r.close();
            p.close();

            //littles
            p = c.prepareStatement("select `brother`.`firstname`,`brother`.`lastname` from `brother` where `brother`.`bigBrotherID`=?");
            p.setInt(1, id);
            p.execute();
            r = p.getResultSet();

            while(r.next()){
                model.LittleBrothers.add(r.getString("firstname") + ' ' + r.getString("lastname"));
            }
            r.close();
            p.close();

            return ResponseUtil.CreateCorsResponse(model, HttpStatus.OK);
        }catch(Exception e){
            System.err.println(e.getMessage());
            return ResponseUtil.CreateCorsResponse(new ErrorModel(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
