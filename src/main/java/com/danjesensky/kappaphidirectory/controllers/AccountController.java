package com.danjesensky.kappaphidirectory.controllers;

import com.danjesensky.kappaphidirectory.models.AuthenticationModel;
import com.danjesensky.kappaphidirectory.models.AuthenticationResponseModel;
import com.danjesensky.kappaphidirectory.models.ErrorModel;
import com.danjesensky.kappaphidirectory.models.SessionModel;
import com.danjesensky.kappaphidirectory.util.DataSourceUtil;
import com.danjesensky.kappaphidirectory.util.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@RestController
public class AccountController {
    private static HashMap<String, SessionModel> sessions;

    static {
        sessions = new HashMap<>();
    }

    @RequestMapping(value = "/api/Account/Authenticate", method = RequestMethod.POST)
    public ResponseEntity authenticate(@RequestBody AuthenticationModel authModel){
        try{
            try(Connection c = DataSourceUtil.getInstance().getDatasource().getConnection()){
                PreparedStatement p = c.prepareStatement("select `id`, `password`, `salt` from `directory`.`account` where `username` = ?");
                p.setString(1, authModel.username);
                p.execute();
                try(ResultSet r = p.getResultSet()){
                    if(r.next()){
                        MessageDigest md = MessageDigest.getInstance("SHA-512");
                        md.update(r.getBytes("salt"));
                        byte[] digest = md.digest(authModel.password.getBytes("UTF-8"));
                        StringBuilder sb = new StringBuilder();
                        for(int i = 0; i < digest.length; i++){
                            sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                        }
                        authModel.password = sb.toString();

                        if (verifyPassword(authModel.password, r.getString("password"))) {
                            UUID uid;
                            do{
                                uid = UUID.randomUUID();
                            }while(sessions.containsKey(uid.toString()));

                            AuthenticationResponseModel response = new AuthenticationResponseModel(uid.toString());
                            sessions.put(uid.toString(), new SessionModel(uid.toString(), r.getInt("id"), response.tokenExpiration));
                            return ResponseUtil.CreateCorsResponse(response, HttpStatus.OK);
                        }
                    }
                    return ResponseUtil.CreateCorsResponse(new ErrorModel("Invalid username or password."), HttpStatus.UNAUTHORIZED);
                }
            }
        }catch(Exception e){
            return ResponseUtil.CreateCorsResponse(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public static int getUserIDByUID(String uid){
        if(sessions.containsKey(uid)) {
            SessionModel s = sessions.get(uid);
            Date now = new Date();
            if(s.tokenExpiration.after(now)) {
                s.tokenExpiration = now;
                return s.userID;
            }
            sessions.remove(uid);
        }
        return -1;
    }

    private static boolean verifyPassword(String s1, String s2){
        if(s1 == null || s2 == null || s1.isEmpty() || s2.isEmpty() || s1.length() != s2.length()){
            return false;
        }

        boolean match = true;
        for(int i = 0; i < s1.length(); i++){
            if(s1.charAt(i) != s2.charAt(i)){
                match = false;
            }
        }

        return match;
    }
}
