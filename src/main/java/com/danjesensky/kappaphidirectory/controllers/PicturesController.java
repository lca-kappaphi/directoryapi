package com.danjesensky.kappaphidirectory.controllers;

import com.danjesensky.kappaphidirectory.models.ErrorModel;
import com.danjesensky.kappaphidirectory.util.DataSourceUtil;
import com.danjesensky.kappaphidirectory.util.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;

@RestController
public class PicturesController {
    @RequestMapping(path = "/api/Pictures/{id}", method = RequestMethod.GET)
    public ResponseEntity s(@PathVariable int id){
        try (Connection c = DataSourceUtil.getInstance().getDatasource().getConnection()){
            ResultSet r;

            PreparedStatement p = c.prepareStatement("select `brother`.`picture` from `brother` where `brother`.`id`=?");
            p.setInt(1, id);
            p.execute();
            r = p.getResultSet();

            if (!r.next()) {
                return ResponseUtil.CreateCorsResponse(new ErrorModel("No such brother found with ID " + id), HttpStatus.NOT_FOUND);
            }

            Blob b = r.getBlob(1);
            byte[] picture = b.getBytes(1, (int)b.length());

            r.close();
            p.close();
            c.close();

            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add("Content-Type", "image/png");
            headers.add("Content-Disposition", "inline");

            return ResponseUtil.CreateCorsResponse(picture, headers, HttpStatus.OK);
        }catch(SQLException e){
            return ResponseUtil.CreateCorsResponse(new ErrorModel(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
