package com.danjesensky.kappaphidirectory.util;

import java.util.List;

@SuppressWarnings({"WeakerAccess", "unchecked"})
public class ArrayUtil {
    public static <T> T[] ToArray(List<T> list){
        return list.toArray((T[])new Object[list.size()]);
    }
}
