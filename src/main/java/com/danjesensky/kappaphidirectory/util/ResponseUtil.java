package com.danjesensky.kappaphidirectory.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class ResponseUtil {
    public static <T> ResponseEntity CreateCorsResponse(HttpStatus status){
        return CreateCorsResponse(null, null, status);
    }

    public static <T> ResponseEntity CreateCorsResponse(T body, HttpStatus status){
        return CreateCorsResponse(body, null, status);
    }

    public static <T> ResponseEntity<T> CreateCorsResponse(T body, MultiValueMap<String, String> headers, HttpStatus status){
        if(headers == null){
            headers = new LinkedMultiValueMap<>();
        }

        headers.add("Access-Control-Allow-Origin", "*");

        return new ResponseEntity<>(body, headers, status);
    }
}
