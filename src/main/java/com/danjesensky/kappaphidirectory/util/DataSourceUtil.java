package com.danjesensky.kappaphidirectory.util;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class DataSourceUtil {
    private static DataSourceUtil instance;
    private final MysqlDataSource dataSource;

    public static DataSourceUtil getInstance(){
        if(instance == null){
            instance = new DataSourceUtil();
        }
        return instance;
    }

    public MysqlDataSource getDatasource(){
        return this.dataSource;
    }

    private DataSourceUtil(){
        this.dataSource = new MysqlDataSource();
        this.dataSource.setUser("root");
        this.dataSource.setPassword("root");
        this.dataSource.setUrl("jdbc:mysql://localhost/directory");
        this.dataSource.setPort(1433);
        this.dataSource.setDatabaseName("directory");
    }
}
