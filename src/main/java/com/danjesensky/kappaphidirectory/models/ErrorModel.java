package com.danjesensky.kappaphidirectory.models;

@SuppressWarnings({"WeakerAccess", "unused"})
public class ErrorModel {
    public ErrorModel(String message){
        this.Message = message;
    }

    public final String Message;
}
