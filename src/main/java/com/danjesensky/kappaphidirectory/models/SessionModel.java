package com.danjesensky.kappaphidirectory.models;

import java.util.Date;

public class SessionModel {
    public String sessionToken;
    public int userID;
    public Date tokenExpiration;
    public static final long TOKEN_LIFESPAN = 60*60*1000;

    public SessionModel(String token, int userID, Date expiration){
        this.sessionToken = token;
        this.userID = userID;
        this.tokenExpiration = expiration;
    }

    public void updateExpiration() throws IllegalStateException{
        if(this.tokenExpiration.before(new Date())){
            throw new IllegalStateException("Token was expired, can't renew it.");
        }
        this.tokenExpiration = new Date(System.currentTimeMillis() + TOKEN_LIFESPAN);
    }
}
