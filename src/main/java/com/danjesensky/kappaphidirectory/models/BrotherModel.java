package com.danjesensky.kappaphidirectory.models;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public class BrotherModel {
    public int ID;
    public String Name;
    public Date DateJoined;
    public Date DateInitiated;
    public Date ExpectedGraduation;
    public Integer ZetaNumber;
    public final List<String> CurrentPositions;
    public final List<String> PastPositions;
    public final List<String> Majors;
    public final List<String> Minors;
    public final List<String> LittleBrothers;
    public String BigBrother;
    public final List<AnsweredQuestionModel> Questions;

    public BrotherModel(){
        this.Questions = new LinkedList<>();
        this.CurrentPositions = new LinkedList<>();
        this.PastPositions = new LinkedList<>();
        this.Majors = new LinkedList<>();
        this.Minors = new LinkedList<>();
        this.LittleBrothers = new LinkedList<>();
    }
}
