package com.danjesensky.kappaphidirectory.models;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DataModel<T> {
    public final T Data;

    public DataModel(T t){
        this.Data = t;
    }
}
