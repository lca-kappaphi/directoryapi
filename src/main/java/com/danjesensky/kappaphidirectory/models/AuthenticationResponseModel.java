package com.danjesensky.kappaphidirectory.models;

import java.util.Date;

public class AuthenticationResponseModel {
    public String sessionToken;
    public Date tokenExpiration;

    public AuthenticationResponseModel(String token){
        this.sessionToken = token;
        this.tokenExpiration = new Date(System.currentTimeMillis() + SessionModel.TOKEN_LIFESPAN);
    }
}
