package com.danjesensky.kappaphidirectory.models;

@SuppressWarnings({"WeakerAccess", "unused"})
public class AnsweredQuestionModel {
    public final String Question;
    public final String Answer;

    public AnsweredQuestionModel(String q, String a){
        this.Question = q;
        this.Answer = a;
    }
}
